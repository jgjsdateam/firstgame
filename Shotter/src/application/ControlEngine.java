package application;

import java.text.AttributedCharacterIterator;
import java.util.ArrayList;
import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;

import javafx.util.Duration;

public class ControlEngine implements EventHandler<ActionEvent> {

	private GraphicsContext gc;
	private Scene scene;
	private boolean gameRun;
	private Player ship;
	private Opponents alien;
	private PlayerController shipController;
	private Timeline tLine;
	private Background background;
	private List<Opponents> asteroids;
	private KeyFrame kf;
	private Statistics statusGame;
	private String showScore = "0";
	private String showLife = "5";
	private int lifeInt;
	private int scoreInt;
	private Sound audio = new Sound();

	public int getLifeInt() {
		return lifeInt;
	}
	
	public ControlEngine(Scene scene, GraphicsContext gc, Statistics status) {
		super();
		this.scene = scene;
		this.gameRun = false;
		this.gc = gc;
		this.ship = new Player(5, 100, 100);
		this.shipController = new PlayerController(ship);
		this.alien = new Opponents(10, 200, 200);
		scene.setOnKeyPressed(shipController);
		scene.setOnKeyReleased(shipController);
		this.tLine = new Timeline();
		this.tLine.setCycleCount(Timeline.INDEFINITE);
		this.background = new Background();
		this.asteroids = new ArrayList<>();
		this.generateAsteroids(15);
		this.kf = new KeyFrame(Duration.seconds(1 / 250.0), this);
		tLine.getKeyFrames().add(kf);
		this.statusGame = status;
	}

	public void start() {
		this.gameRun = true;
		tLine.play();

	}

	@Override
	public void handle(ActionEvent arg0) {
		double time = 1.0;
		ship.move();
		gc.clearRect(0, 0, Main.WIDTH, Main.HEIGHT);
		background.draw(gc, time);

		this.checkPlayerColision();
		for (Opponents spaceRock : asteroids) {
			spaceRock.move();
			if ((spaceRock.getPositionX() + spaceRock.getWidth()) <= -100) {
				if (gameRun) {
					statusGame.addScore(spaceRock.getScore());
					lifeInt = ship.getLiveCounter();
					scoreInt = statusGame.getScore();
					showScore = "" + scoreInt;
					showLife = "" + lifeInt;
					if (lifeInt == 0) {
						audio.audioGameOver();	
						audio.audioBackgroundStop();
					}
				}
				asteroids.remove(spaceRock);
				this.generateOneAsteroid();
				this.gameRun = this.ship.CheckPlayerlife();
			} else {
				gc.drawImage(spaceRock.getOpponentsFrame(time), spaceRock.getPositionX(), spaceRock.getPositionY());
			}
		}
		if (this.gameRun == true) {
			
			gc.strokeText("Score: " + showScore, 50, 50);
			gc.strokeText("Life left: " + showLife, 400, 50);
			gc.drawImage(shipController.getPlayerFrame(time), ship.getPositionX(), ship.getPositionY());
		} else {
			Main.endGame(gc, statusGame);
		}

	}
	
	private void generateAsteroids(int number) {

		for (int i = 0; i < number; i++) {
			this.generateOneAsteroid();
		}
	}

	private void generateOneAsteroid() {
		int posx = Main.WIDTH + (int) Math.floor(Math.random() * Main.WIDTH);
		int posy = (int) Math.floor(Math.random() * Main.HEIGHT);
		int speed = (int) Math.floor(Math.random() * 4 + 1);
		asteroids.add(new Opponents(speed, posx, posy));
	}

	private void checkPlayerColision() {
		for (Opponents astro : asteroids) {
			if (astro.intersects(ship)) {
				ship.loseLife(1);
				asteroids.remove(astro);
				this.generateOneAsteroid();
				System.out.println(ship.getLiveCounter());
				if (gameRun) {
					audio.audioCollisionPlay();
				}
			}
		}
	}


}
