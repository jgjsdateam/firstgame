package application;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Background {

	private Sprite background;
	private int width;
	private int height;
	private int posX;
	private int posY;
	private int velocity;

	public Background() {
		this.background = new Sprite();
		this.height = Main.HEIGHT;
		this.width = Main.WIDTH;
		this.posX = 0;
		this.posY = 0;
		this.velocity = 1;
		initImage();
	}

	private void initImage() {
		background.addImage("res/background/default.png");

	}

	public Image getImage(double time) {
		return this.background.getFrame(time);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void draw(GraphicsContext gc, double time) {

		if (posX + this.width > 0) {
			posX -= velocity;
		} else {
			posX = 0;
		}
		for (int tempX = this.posX; tempX <= Main.WIDTH; tempX += this.width) {

			gc.drawImage(this.getImage(time), tempX, this.posY, this.width, this.height);
		}
	}

}
